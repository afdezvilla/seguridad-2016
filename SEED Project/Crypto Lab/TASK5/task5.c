#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>

/****************************************
*					*
*  Realizado por :			*
*					*
*		Álvaro Fernández Villa	*
*					*
****************************************/

// Metodo para convertir de binario a hexadecimal
void binaryToHex(unsigned char *outbuf, char *buffer, int ciphertext_len) 
{
	int i;
	char* bufferAux = buffer;
	
	for (i = 0; i < ciphertext_len; i++)
	{
		// Hace la conversión a Hexadecimal
		bufferAux += sprintf(bufferAux, "%02X", outbuf[i]);
	}
}

// Metodo para convertir de hexadecimal a binario
int hexToBinary(unsigned char *buffer, const char *ciphertext, int len)
{
	int i = 0;
	int j = 0;

    int ciphertext_len = strlen(ciphertext);
    int buffer_size = (ciphertext_len+1)/2;

    // Comprueba que no supera el tamaño designado
    if (buffer_size > len)
    {
        return -1;
    }
    // Comprueba si la longuitud es impar 
    if (ciphertext_len % 2 == 1)
    {
        // Si es impar asume que existe el prefijo '0'
        if (sscanf(&(ciphertext[0]), "%1hhx", &(buffer[0])) != 1)
        {
            return -1;
        }

        i = j = 1;
    }
    // Recorre el resto de la cadena
    for (; i < ciphertext_len; i+=2, j++)
    {
    	// Hace la conversión a Binario
        if (sscanf(&(ciphertext[i]), "%2hhx", &(buffer[j])) != 1)
        {
            return -1;
        }
    }
    return 1;
}


int encrypt(char *palabra, unsigned char *iv, char *text, char *ciphertext)
{
	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	unsigned char outbuf[1024];
	int ciphertext_len;
	int len;
	int i;

	// Inicia el proceso de encriptado
	EVP_EncryptInit_ex(&ctx, EVP_aes_128_cbc(), NULL, palabra, iv);
		
	// Encripta el mensaje y lo devuelve encriptado	
	if(!EVP_EncryptUpdate(&ctx, outbuf, &ciphertext_len, text, strlen ((char *)text)))
	{
		printf("ERROR en EVP_EncryptUpdate \n");
	}

	// Finaliza el proceso de encriptado
	if(!EVP_EncryptFinal_ex(&ctx, outbuf + ciphertext_len, &len))
	{
		printf("ERROR en EVP_EncryptFinal_ex \n");
	} 
	ciphertext_len += len;

	// Crea el buffer para convertir el encriptado binario a hexadecimal
	char* buffer = malloc(2*ciphertext_len + 1); 
	binaryToHex(outbuf, buffer, ciphertext_len);

	// Imprime información 
	/*printf( "Tamaño Ciphertext Original : %lu \n", strlen(ciphertext));
	printf( "Ciphertext Original : %s \n", ciphertext); 
	printf( "Tamaño Ciphertext Generado : %lu \n", strlen(buffer));
	printf( "Ciphertext Generado : %s \n", buffer);*/

	// Compara las cadenas para saber si es la palabra utilizada
	if(strcmp(ciphertext,buffer) == 0)
	{
		printf("Encontrada\n");
		printf("Palabra: %s \n", palabra);
		exit(0);
	}
}

int decrypt(char *palabra, unsigned char *iv, char *text, char *ciphertext)
{
	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	unsigned char outbuf[2048];
	unsigned char buffer[2048];
	int plaintext_len;
	int len;
	int i;

	// Convierte el texto en hexadecimal a binario
	hexToBinary(buffer, ciphertext, sizeof(buffer));

	// Inicia el proceso de desencriptado
	EVP_DecryptInit_ex(&ctx, EVP_aes_128_cbc(), NULL, palabra, iv);
			
	// Desencripta el mensaje y lo devuelve desencriptado
	if(!EVP_DecryptUpdate(&ctx, outbuf, &plaintext_len, (const unsigned char*) buffer, strlen(buffer)))
	{
		printf("ERROR en EVP_EncryptUpdate \n");
	}

	// Finaliza el proceso de desencriptado
	if(!EVP_DecryptFinal_ex(&ctx, outbuf + plaintext_len, &len))
	{
		//printf("ERROR en EVP_EncryptFinal_ex \n");
	} 
	plaintext_len += len;

	// Elimina el padding incluido en el cifrado
	if(strlen(outbuf) > strlen(text))
	{ 
		for(i = strlen(text); i < strlen(outbuf); i++)
		{
			outbuf[i] = '\0';
		}
	}

	// Imprime información 
	/*printf( "Tamaño Text Original : %lu \n", strlen(text)); 
	printf( "Text Original : %s \n", text); 
	printf( "Tamaño Text Generado : %lu \n", strlen(outbuf)); 
	printf( "Text Generado : %s \n", outbuf);*/

	// Compara las cadenas para saber si es la palabra utilizada
	if(strcmp(outbuf,text) == 0)
	{
		printf("Encontrada\n");
		printf("Palabra: %s \n", palabra);
		exit(0);
	}

}

int main()
{
	int i;
	int choice;
	char eleccion[7];
	FILE *fichero;
	char palabra[16];
	char textOriginal[] = "This is a top secret.";
	char ciphertextOriginal[] =  "8D20E5056A8D24D0462CE74E4904C1B513E10D1DF4A2EF2AD4540FAE1CA0AAF9";
	unsigned char iv[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	
	printf("\nÁlvaro Fernández Villa\n");
	printf("Secret-Key Encryption\n");
	printf("[1] Modo Encrypt\n");
	printf("[2] Modo Decrypt\n");
	printf("[3] Salir\n");
	scanf("%i", &choice);

	// Menu
	switch (choice)
	{
	case 1: 
		strcpy(eleccion,"Encrypt");
	    break;
	case 2:
		strcpy(eleccion,"Decrypt");
	    break;
	case 3:
		exit(0);
	    break;
	}

	// Abre el fichero
	fichero = fopen("words.txt","r");
	// Control de errores
	if(fichero == 0){
		perror("No se pudo abrir el archivo");
		exit(-1);
	}
	else
	{
		while ((fgets(palabra,16,fichero)))
		{	
			// Susutituye el ultimo caracter de la palabra
			palabra[strlen(palabra) - 1] = '\0';
			// Rellena la palabra con espacios
			if(strlen(palabra) < 16)
			{ 
				for(i = strlen(palabra); i < 16; i++)
				{
					palabra[i] = ' ';
				}
			}

			// Imprime información 
			/*printf("\n");
			printf("Palabra: %s \n", palabra);*/

			// Eleccion modo Encriptado
			if(strcmp(eleccion,"Encrypt") == 0){
				if(!encrypt(palabra, iv, textOriginal, ciphertextOriginal))
				{
					printf("ERROR en Encrypt \n");
				}
			}

			// Eleccion modo Desencriptado
			if(strcmp(eleccion,"Decrypt") == 0){
				if(!decrypt(palabra, iv, textOriginal, ciphertextOriginal))
				{
					printf("ERROR en Decrypt \n");
				}
			}

		}
	}

	fclose(fichero);
	return 0;
}


