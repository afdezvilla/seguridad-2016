#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/types.h>
#include <linux/byteorder/generic.h>
#include <linux/string.h>

//Definimos las variables constantes
#define ATACANTEIP "10.0.2.12"
#define VICTIMAIP "10.0.2.11"
#define TWITTERIP "104.244.42.1"
#define TAMANOIP 15

// Registramos las funciones de entrada y salida 
static struct nf_hook_ops nfhoIn;
static struct nf_hook_ops nfhoOut;

struct sk_buff *sock_buff;

// Declaramos las cabeceras de los paquetes
struct tcphdr *headerTCP;              
struct iphdr *headerIP;                

// Metodo gancho/hook entrada
unsigned int in_hook_func(unsigned int hooknum, struct sk_buff **skb, const struct net_device *in, const struct net_device *out, int (okfn)(struct sk_buff *)){

	sock_buff = skb;
	// Inicializamos la cabecera IP con la cabecera del paquete
	headerIP = (struct iphdr *)skb_network_header(sock_buff);
	if(!sock_buff) return NF_ACCEPT;
	
	// Declaramos las variables que se utilizaran
	char *atacanteIP = ATACANTEIP;  
	char *victimaIP = VICTIMAIP;   
	unsigned int puerto; 
	char *origenIP[TAMANOIP];
	char *destinoIP[TAMANOIP];;

	// Si la cabecera contiene el protocolo TCP
	if (headerIP->protocol == IPPROTO_TCP) {
		// Regla 1 - Prevenir que A haga telnet a B
		// Convertimos la cabecera del paquete y la guardamos
		snprintf(origenIP, TAMANOIP, "%pI4", &headerIP->saddr);
		snprintf(destinoIP, TAMANOIP, "%pI4", &headerIP->daddr);
		// Comparamos las ip's
		if((strcmp(origenIP,atacanteIP) == 0) && (strcmp(destinoIP, victimaIP) == 0)){
			// Fix para paquetes entrantes
			headerTCP = (struct tcphdr *)((__u32 *)headerIP + headerIP->ihl);
			//headerTCP = (struct tcphdr *)skb_transport_header(skb);
			// Inicializamos el puerto con la conversion del puerto obtenido de la cabecera
			puerto = htons((unsigned short int) headerTCP->dest);
			// Si el puerto es el de telnet rechazamos los paquetes
			if(puerto == 23){ 
				printk(KERN_INFO "Regla 1 - Prevenir que A haga telnet a B\n");
				return NF_DROP;
			}	
		}
	}
	return NF_ACCEPT; //Si no se cumple nada, acepto paquetes
}

// Metodo gancho/hook salida
unsigned int out_hook_func(unsigned int hooknum, struct sk_buff **skb, const struct net_device *in, const struct net_device *out, int (okfn)(struct sk_buff *)){    
 
	sock_buff = skb;
	// Inicializamos la cabecera IP con la cabecera del paquete
	headerIP = (struct iphdr *)skb_network_header(sock_buff);
	if(!sock_buff) return NF_ACCEPT;
	
	// Declaramos las variables que se utilizaran
	char *atacanteIP = ATACANTEIP;  
	char *victimaIP = VICTIMAIP;  
	char *twitterIP = TWITTERIP; 
	unsigned int puerto; 
	char *origenIP[TAMANOIP];
	char *destinoIP[TAMANOIP];

	// Si la cabecera contiene el protocolo TCP
	if (headerIP->protocol == IPPROTO_TCP){ 
		// Regla 2 - Prevenir que B haga telnet a A
		// Convertimos la cabecera del paquete y la guardamos
		snprintf(origenIP, TAMANOIP, "%pI4", &headerIP->saddr);
		snprintf(destinoIP, TAMANOIP, "%pI4", &headerIP->daddr);
		// Comparamos las ip's
		if((strcmp(origenIP, victimaIP) == 0) && (strcmp(destinoIP, atacanteIP) == 0)){
			// Inicializamos la cabecera TCP
			headerTCP = (struct tcphdr *)skb_transport_header(skb);
			// Inicializamos el puerto con la conversion del puerto obtenido de la cabecera
			puerto = htons((unsigned short int) headerTCP->dest);
			// Si el puerto es el de telnet rechazamos los paquetes
			if(puerto == 23){
				printk(KERN_INFO "Regla 2 - Prevenir que B haga telnet a A\n");
				return NF_DROP;
			}		
		}
		// Regla 4 - Prevenir que B haga SSH
		// Comparamos las ip's
		if(strcmp(origenIP, victimaIP) == 0){
			// Inicializamos la cabecera TCP
			headerTCP = (struct tcphdr *)skb_transport_header(skb);
			// Inicializamos el puerto con la conversion del puerto obtenido de la cabecera
			puerto = htons((unsigned short int) headerTCP->dest);
			// Si el puerto es el de ssh rechazamos los paquetes
			if(puerto == 22){ 
				printk(KERN_INFO "Regla 4 - Prevenir que B haga SSH\n");
				return NF_DROP;
			}	
		}
	}
	// Si la cabecera contiene el protocolo ICMP
	if (headerIP->protocol==IPPROTO_ICMP){ 
		// Regla 3 - Prevenir que B haga ping a web externa
		// Convertimos la cabecera del paquete y la guardamos
		snprintf(origenIP, TAMANOIP, "%pI4", &headerIP->saddr);
		snprintf(destinoIP, TAMANOIP, "%pI4", &headerIP->daddr);
		// Comparamos las ip's
		if((strcmp(destinoIP, twitterIP) == 0) && (strcmp(origenIP, victimaIP) == 0)){
			printk(KERN_INFO "Regla 3 - Prevenir que B haga ping a web externa\n");
			return NF_DROP;
		}
		// Regla 5 - Prevenir que B haga ping a A
		// Comparamos las ip's
		if((strcmp(origenIP, victimaIP) == 0) && (strcmp(destinoIP, atacanteIP) == 0)){
			printk(KERN_INFO "Regla 5 - Prevenir que B haga ping a A\n");
			return NF_DROP;
		}
	}
	return NF_ACCEPT; //Si no se cumple nada, acepto paquetes
}

int init_module(){
	//Entrada
	nfhoIn.hook = in_hook_func;
	nfhoIn.hooknum = NF_INET_LOCAL_IN;
	nfhoIn.pf = PF_INET;
	nfhoIn.priority = NF_IP_PRI_FIRST;

	nf_register_hook(&nfhoIn);
	
	//Salida
	nfhoOut.hook = out_hook_func;
	nfhoOut.hooknum = NF_INET_LOCAL_OUT;
	nfhoOut.pf = PF_INET;
	nfhoOut.priority = NF_IP_PRI_FIRST;

	nf_register_hook(&nfhoOut);

	return 0;
}

void cleanup_module(){
	nf_unregister_hook(&nfhoIn); //Entrada
	nf_unregister_hook(&nfhoOut); //Salida
}


